FROM php:7.2-apache

LABEL maintainer="gabriel@gabrielfringeli.ch"

# add php configuration
COPY .Docker/php.ini /usr/local/etc/php/

# add apache site configuration
COPY .Docker/000-default.conf /etc/apache2/sites-enabled/

# install Apache Rewrite module
RUN a2enmod rewrite

# install sudo
RUN apt-get update && apt-get install sudo unzip

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install git
RUN apt-get update && apt-get install -y git

# install ImageMagick
RUN apt-get install -y --no-install-recommends \
        libmagickwand-dev \
    && rm -rf /var/lib/apt/lists/*
RUN pecl install imagick-3.4.3 \
    && docker-php-ext-enable imagick

# install Intl extension
RUN apt-get update && apt-get install -y libicu-dev
RUN docker-php-ext-install intl

# install PDO MySQL driver PHP extension
RUN docker-php-ext-install pdo_mysql

# install npm
RUN apt-get install -y -qq npm

# install bower
RUN npm install --global bower

#
# add composer files, setup project
#
ADD . /var/www/html/
RUN chown www-data:www-data -R ..
USER www-data
RUN composer install
RUN rm /var/www/html/auth.json
RUN cd /var/www/html/Packages/Sites/GF.CCLaufen && bower install
RUN cd /var/www/html/Packages/Application/GF.Calendar && bower install

USER root
